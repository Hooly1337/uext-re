import { createEvent } from 'effector'
import { Mode } from '../config'

export const modeChanged = createEvent<Mode>()

export const profileAdded = createEvent<string>()

export const profileChanged = createEvent<string>()
