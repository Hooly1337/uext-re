import { createStore } from 'effector'
import { Mode } from '../config'
import { StorageAPI } from 'shared/lib'
import { modeChanged, profileAdded, profileChanged } from './effects'

const STORAGE_KEY = 'modes'
const NEW_PROFILE = 'new'

interface Profiles {
  current: string
  all: string[]
}

type ModeParams = Partial<Record<Mode, Profiles>> & { currentMode: Mode }

const modeParamsFactory = (): ModeParams => {
  return {
    currentMode: Mode.None,
    [Mode.Floats]: {
      current: NEW_PROFILE,
      all: []
    },
    [Mode.Stickers]: {
      current: NEW_PROFILE,
      all: []
    }
  }
}

const initialModeParams = (): ModeParams => StorageAPI.getJson<ModeParams>(STORAGE_KEY) ?? modeParamsFactory()

const $mode = createStore<ModeParams>(initialModeParams())
  .on(modeChanged, (state, newMode) => ({ ...state, currentMode: newMode }))
  .on(profileAdded, (state, profileName) => ({ ...state, [state.currentMode]: { current: profileName, all: [...state[state.currentMode]!.all, profileName] } }))
  .on(profileChanged, (state, profileName) => ({ ...state, [state.currentMode]: { current: profileName, all: [...state[state.currentMode]!.all] } }))

$mode.watch(state => { StorageAPI.setJson(STORAGE_KEY, state) })

const $currentModeProfiles = $mode.map(state => state[state.currentMode] as Profiles)

const $currentMode = $mode.map(state => state.currentMode)

export const ModeModel = {
  $mode,
  $currentModeProfiles,
  $currentMode,
  modeChanged,
  profileAdded,
  profileChanged,
  NEW_PROFILE
}
