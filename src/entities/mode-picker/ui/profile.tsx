import { Button, Input, Select } from 'antd'
import { useStore } from 'effector-react'
import { ReactElement, useState } from 'react'
import { ModeModel } from '../model'

const { Option } = Select

export const ProfileSelector = (): ReactElement => {
  const currentModeProfiles = useStore(ModeModel.$currentModeProfiles)

  return (
        <>
            <Select value={currentModeProfiles.current} onChange={ModeModel.profileChanged} style={{ width: 170 }}>
                <Option value={ModeModel.NEW_PROFILE}>add new</Option>
                {
                    currentModeProfiles.all.map((profile) => (<Option key={profile} value={profile}>{profile}</Option>))
                }
            </Select>
            { currentModeProfiles.current == ModeModel.NEW_PROFILE &&
                <AddProfileForm/>
            }
        </>
  )
}

const AddProfileForm = (): ReactElement => {
  const [profileInput, setProfileInput] = useState<string>('')

  const add = (): void => {
    ModeModel.profileAdded(profileInput)
  }
  return (
        <>
            <Input style={{ width: 200 }} onChange={(e) => setProfileInput(e.target.value)}/>
            <Button onClick={add}>add</Button>
        </>
  )
}
