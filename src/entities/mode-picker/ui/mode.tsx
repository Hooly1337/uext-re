import { Select } from 'antd'
import { ModeModel, Mode } from 'entities/mode-picker/index'
import { useStore } from 'effector-react'
import { ReactElement } from 'react'
import { ProfileSelector } from './profile'

const { Option } = Select

export const ModeSelector = (): ReactElement => {
  const mode = useStore(ModeModel.$currentMode)

  return (
      <>
          <Select defaultValue={mode} onChange={ModeModel.modeChanged} style={{ width: 170 }}>
              <Option value={Mode.Stickers}>Stickers</Option>
              <Option value={Mode.Floats}>Floats</Option>
          </Select>
          <ProfileSelector/>
      </>
  )
}
