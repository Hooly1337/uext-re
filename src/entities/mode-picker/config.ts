export enum Mode {
  Stickers = 'stickers',
  Floats = 'floats',
  None = 'none'
}
