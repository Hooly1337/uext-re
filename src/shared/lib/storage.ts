const get = <T = string>(name: string): T | null => {
  return localStorage.getItem(name) as T
}

const getJson = <T>(name: string): T | null => {
  const item = localStorage.getItem(name)
  if (item == null) return null

  try {
    return JSON.parse(item)
  } catch (e) {
    console.error(e)
    return null
  }
}

const set = (name: string, value: string): void => {
  localStorage.setItem(name, value)
}

const setJson = (name: string, value: any): void => {
  localStorage.setItem(name, JSON.stringify(value))
}

export const StorageAPI = {
  get,
  set,
  setJson,
  getJson
}
