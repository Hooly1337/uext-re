import { Table } from 'antd'
import classNames from 'classnames'
import React from 'react'
// @ts-expect-error
import { VariableSizeGrid as Grid } from 'react-window'

export const VirtualTable = (props: Parameters<typeof Table>[0]) => {
  const { columns, scroll } = props

  const renderVirtualList = (rawData: object[], { scrollbarSize, onScroll }: any) => {
    const totalHeight = rawData.length * 54

    return (
      <Grid
        className="virtual-grid"
        columnCount={columns!.length}
        columnWidth={(index: number) => {
          const { width } = columns![index]
          return totalHeight > scroll!.y! && index === columns!.length - 1
            ? (width as number) - scrollbarSize - 1
            : (width as number)
        }}
        height={scroll!.y as number}
        rowCount={rawData.length}
        width={window.innerWidth}
        rowHeight={() => 54}
        onScroll={({ scrollLeft }: { scrollLeft: number }) => {
          onScroll({ scrollLeft })
        }}
      >
        {({
          columnIndex,
          rowIndex,
          style
        }: {
          columnIndex: number
          rowIndex: number
          style: React.CSSProperties
        }) => (
          <div
            className={classNames('virtual-table-cell', {
              'virtual-table-cell-last': columnIndex === columns!.length - 1
            })}
            style={style}
          >
            {(rawData[rowIndex] as any)[(columns as any)[columnIndex].dataIndex]}
          </div>
        )}
      </Grid>
    )
  }

  return (

      <Table
        {...props}
        className="virtual-table"
        columns={columns}
        pagination={false}
        components={{
          // @ts-expect-error
          body: renderVirtualList
        }}
      />
  )
}
