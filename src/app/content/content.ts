export interface ItemInfo {
  type: 'ExportItem'
  name: string
}

export function main (): void {
  const itemActions = document.querySelector('#largeiteminfo_item_actions')
  if (itemActions != null) {
    const button = document.createElement('a')
    button.classList.add('btn_grey_white_innerfade')
    button.classList.add('btn_small')

    const span = document.createElement('span')
    const content = document.createTextNode('Add to fav')

    span.appendChild(content)
    button.appendChild(span)

    button.addEventListener('click', () => {
      const path = location.pathname.split('/')
      const name = decodeURIComponent(path[path.length - 1])
      const info: ItemInfo = { type: 'ExportItem', name }
      void chrome.runtime.sendMessage(info)
    })
    itemActions.append(button)
  }
}
