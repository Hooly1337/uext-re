import React from 'react'
import { Mode, ModeModel } from 'entities/mode-picker'
import { StickersMode } from 'pages/stickers'
import { FloatsMode } from 'pages/floats'
import { useStore } from 'effector-react'

export const ModeRouter = (): React.ReactElement | null => {
  const mode = useStore(ModeModel.$currentMode)

  switch (mode) {
    case Mode.Stickers:
      return <StickersMode/>
    case Mode.Floats:
      return <FloatsMode/>
    case Mode.None: return null
  }
}
