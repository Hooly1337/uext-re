import { ModeSelector } from 'entities/mode-picker/ui/mode'
import { ModeRouter } from './hoc'

import './index.scss'
import { ReactElement } from 'react'

function App (): ReactElement {
  return (
      <>
        <ModeSelector/>
        <ModeRouter/>
      </>
  )
}

export default App
