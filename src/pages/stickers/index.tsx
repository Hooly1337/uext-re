import React from 'react'
import { VirtualTable } from 'shared/ui/table'

const columns = [
  { title: 'Name', dataIndex: 'name', width: 150 },
  { title: 'Steam', dataIndex: 'link', width: 150 },
  { title: 'Price', dataIndex: 'price', width: 150 },
  { title: 'Max price(%)', dataIndex: 'maxPricePercent', width: 150 },
  { title: '1', dataIndex: 'streak1', width: 150 },
  { title: '2', dataIndex: 'streak2', width: 150 },
  { title: '3', dataIndex: 'streak3', width: 150 },
  { title: '4', dataIndex: 'streak4', width: 150 }
]

const data = Array.from({ length: 100000 }, (_, key) => ({ key, name: 'Lorem asD  wer wfESF ESWF S FSDF sdF SEewF fewFWEAF', link: 'someLink', price: 1337, maxPricePercent: 20, streak1: 1000, streak2: 1200, streak3: 1400, streak4: 1700 }))

export const StickersMode: React.FC = () => {
  return (
    <VirtualTable columns={columns} dataSource={data} scroll={{ y: (window.innerHeight * 0.9) }} />
  )
}
