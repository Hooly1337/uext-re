import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { viteStaticCopy } from 'vite-plugin-static-copy'
import { resolve } from 'path'
import tsconfigPaths from 'vite-tsconfig-paths';

const src = resolve(__dirname, 'src');

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        react({
            babel: {
                plugins: ['effector/babel-plugin']
            }
        }),
        viteStaticCopy({
            targets: [{
                src: 'manifest.json | background-wrapper.js',
                dest: '/'
            }]
        }),
        tsconfigPaths()
    ],
    build: {
        outDir: "extension",
        rollupOptions: {
            input: {
                content: resolve(src, 'app', 'content', 'index.ts'),
                background: resolve(src, 'app', 'background', 'index.ts'),
                app: resolve('index.html')
            },
            output: {
                entryFileNames: chunk => `dist/${chunk.name}/index.js`
            }
        }
    }
})
