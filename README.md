## content scripts
`src/app/content`

## service-worker
`src/app/background`

## manifest v3
`public/manifest.json`

## application
`src/app/index.tsx`